context("Selection of snapshots")


# This is some altered sample data
dat_string <- "
'urlkey' 'timestamp' 'original' 'mimetype' 'statuscode' 'digest' 'length' 'date'
'com,abeking)/' '19980131045322' 'http://www.abeking.com:80/' 'text/html' '200' '5THJZMZRYHLKH5PXYL7JCUABGXA6QBTS' 4747 1998-01-31
'com,abeking)/' '19981212022439' 'http://www.abeking.com:80/' 'text/html' '200' 'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK' 5847 1998-12-12
'com,abeking)/' '19990125092929' 'http://www.abeking.com:80/' 'text/html' '200' 'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK' 5848 1999-01-25
'com,abeking)/' '19990129010449' 'http://www.abeking.com:80/' 'text/html' '200' 'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK' 5846 1999-01-29
'com,abeking)/' '19990208011102' 'http://www.abeking.com:80/' 'text/html' '200' 'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK' 5847 1999-02-08
'com,abeking)/' '19990421141640' 'http://www.abeking.com:80/' 'text/html' '200' '47ZUFITBNJE3VHT3RJDTDNGHPPLKTRSN' 5851 1999-04-21
'com,abeking)/' '19990427051529' 'http://www.abeking.com:80/' 'text/html' '200' '47ZUFITBNJE3VHT3RJDTDNGHPPLKTRSN' 5852 1999-04-27
'com,abeking)/' '20000301140813' 'http://www.abeking.com:80/' 'text/html' '200' 'J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE' 5907 2000-03-01
'com,abeking)/' '20000301140813' 'http://www.abeking.com:80/' 'text/html' '200' 'J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE' 5907 2000-03-01
'com,abeking)/' '20000510202931' 'http://www.abeking.com:80/' 'text/html' '200' 'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC' 5973 2000-05-10
'com,abeking)/' '20000511031234' 'http://www.abeking.com:80/' 'text/html' '200' 'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC' 5976 2000-05-11
'com,abeking)/' '20000520072426' 'http://www.abeking.com:80/' 'text/html' '200' 'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC' 5973 2000-05-20
'com,abeking)/' '20000818132318' 'http://www.abeking.com:80/' 'text/html' '200' '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q' 5978 2000-08-18
'com,abeking)/' '20001017141206' 'http://www.abeking.com:80/' 'text/html' '200' '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q' 5980 2000-10-17
'com,abeking)/' '20001017141206' 'http://www.abeking.com:80/' 'text/html' '200' 'manually_changed_to_provide_warn' 5980 2000-10-17
'com,abeking)/' '20001018034440' 'http://www.abeking.com:80/' 'text/html' '200' '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q' 5981 2000-10-18
'com,abeking)/' '20001019032613' 'http://www.abeking.com:80/' 'text/html' '200' '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q' 5980 2000-10-19
'com,abeking)/' '20001019040456' 'http://www.abeking.com:80/' 'text/html' '200' '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q' 5980 2000-10-19
'com,abeking)/' '20001202032700' 'http://www.abeking.com:80/' 'text/html' '200' '4ZQ5EPTHDMUO5X46MFXQJGWOVRP7RQTM' 6005 2000-12-02
"
cdx <- read.table(text = dat_string, header = T)
cdx$date <- as.Date(cdx$date)

# I want to see if the select date function returns the correct digests and groups
# I check digests since this is easier than testing for the entire dataframe.
# The options are:
#   One or multiple dates
#   exact or non-exact matches
#   Further I need to check the warning for ambigous matching
#
# first a single exact match
d1 <- as.Date("2000-05-10")
dr1 <- data.frame(digest = "ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC",
                  group = 1)
# two exact matches
d2 <- as.Date(c("2000-05-11", "2000-05-20"))
dr2 <- data.frame(digest = c("ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC",
                             "ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC"),
                  group = c(1, 2))
# one non-exact match
d3 <- as.Date("1999-04-26")
dr3 <- data.frame(digest = "47ZUFITBNJE3VHT3RJDTDNGHPPLKTRSN",
                  group = 1)
# and two non-exact matches
d4 <- as.Date(c("1999-04-26", "2000-08-10"))
dr4 <- data.frame(digest = c("47ZUFITBNJE3VHT3RJDTDNGHPPLKTRSN",
                             "5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q"),
                  group = c(1, 2))
# Checking extreme dates
d5 <- as.Date(c("2030-01-01", "1000-01-01"))
dr5 <- data.frame(digest = c("4ZQ5EPTHDMUO5X46MFXQJGWOVRP7RQTM",
                             "5THJZMZRYHLKH5PXYL7JCUABGXA6QBTS"),
                  group = c(1, 2))
# and finally checking that I get the warning when I've got an ambigous date
d6 <- as.Date("2000-10-17")
test_that("Selection based on date", {
    expect_equal(WaybackFetcheR:::select_snaps_date(cdx, d1)[, c("digest", "group")], dr1)
    expect_equal(WaybackFetcheR:::select_snaps_date(cdx, d2)[, c("digest", "group")], dr2)
    expect_equal(WaybackFetcheR:::select_snaps_date(cdx, d3)[, c("digest", "group")], dr3)
    expect_equal(WaybackFetcheR:::select_snaps_date(cdx, d4)[, c("digest", "group")], dr4)
    expect_equal(WaybackFetcheR:::select_snaps_date(cdx, d5)[, c("digest", "group")], dr5)
    expect_warning(WaybackFetcheR:::select_snaps_date(cdx, d6))
})


# Now I want to run the same tests for the years
# Again I want to test:
#   one and multiple
#   matching and missing dates
#   Furthermore I need to test that if there is output for one year missing,
#   the return is okay.
#
# A single year, the date is at the beginning of the year
y1 <- as.Date("2000-01-01")
yr1 <- data.frame(digest = "J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE", 
                  group = 1)
# a single year, with the date at the end of the year.
y2 <- as.Date("2000-10-10")
yr2 <- data.frame(digest = "J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE", 
                  group = 1)
# The same two tests, but with two years. Both exist
y3 <- as.Date(c("1999-01-01", "2000-01-01"))
yr3 <- data.frame(digest = c("J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE",
                             "VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK"),
                  group = c(2, 1))
y4 <- as.Date(c("1999-10-10", "2000-10-10"))
yr4 <- data.frame(digest = c("J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE",
                             "VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK"),
                  group = c(2, 1))
# Here I want to test that a nonexisting year is treated correctly.
y5 <- as.Date(c("1999-01-01", "2030-01-01", "2000-01-01"))
yr5 <- data.frame(digest = c("J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE",
                             "VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK"),
                  group = c(3, 1))
# and Finally the behaviour when no year exists
y6 <- as.Date(c("2100-01-01", "2101-01-01"))
# executing the tests
test_that("Selection based on year", {
    expect_equal(WaybackFetcheR:::select_snaps_year(cdx, y1)[, c("digest", "group")], yr1)
    expect_equal(WaybackFetcheR:::select_snaps_year(cdx, y2)[, c("digest", "group")], yr2)
    expect_equal(WaybackFetcheR:::select_snaps_year(cdx, y3)[, c("digest", "group")], yr3)
    expect_equal(WaybackFetcheR:::select_snaps_year(cdx, y4)[, c("digest", "group")], yr4)
    expect_equal(WaybackFetcheR:::select_snaps_year(cdx, y5)[, c("digest", "group")], yr5)
    expect_equal(WaybackFetcheR:::select_snaps_year(cdx, y6), NULL)
})


# And finally the testing code for time periods.
# I need to test that the results are correct when:
#   1 group (empty and full)
#   2 groups (both full, both empty, one full / empty, different orientation)
#   If the n parameter works
#   If the invert selection works.
#   I have to thest that the behaviour is constant when the dates are equal to the borders.
#
# First testing one group:
#   with one result
ps1 <- as.Date("1998-12-01")
pe1 <- as.Date("1998-12-30")
pr1 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK'),
                  group = c(1))
# with several results.
ps2 <- as.Date("1999-01-01")
pe2 <- as.Date("1999-03-01")
pr2 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK'),
                  group = c(1, 1, 1))
# and an empty group
ps3 <- as.Date("2010-01-01")
pe3 <- as.Date("2013-01-01")
pr3 <- NULL
#
# and now the same with two groups
# two simple groups with one hit each.
ps4 <- as.Date(c("1998-12-01", "2000-05-12"))
pe4 <- as.Date(c("1998-12-30", "2000-05-30"))
pr4 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC'),
                  group = c(1, 2))
# two groups with multiple hits
ps5 <- as.Date(c("1999-01-01", "2000-05-01"))
pe5 <- as.Date(c("1999-03-01", "2000-05-30"))
pr5 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC'),
                  group = c(1, 1, 1, 2, 2, 2))
# now I need to test if empty groups are handled correctly
# first group empty
ps6 <- as.Date(c("2010-01-01", "2000-05-01"))
pe6 <- as.Date(c("2010-03-01", "2000-05-30"))
pr6 <- data.frame(digest = c('ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC'),
                  group = c(2, 2, 2))
# second group empty
ps7 <- as.Date(c("1999-01-01", "2010-05-01"))
pe7 <- as.Date(c("1999-03-01", "2010-05-30"))
pr7 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK'),
                  group = c(1, 1, 1))
# Dates that are equal to either start or end should be used.
# This is tested here. Further if there are two (equal) snaps per 
# day both should be returned.
ps8 <- as.Date("2000-03-01")
pe8 <- as.Date("2000-05-10")
pr8 <- data.frame(digest = c('J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE',
                             'J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC'),
                  group = c(1, 1, 1))
# confirm that n=2 results in only two results
# for only one group
ps9 <- as.Date("1999-01-01")
pe9 <- as.Date("1999-03-01")
pr9 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK'),
                  group = c(1, 1))
# and if there are two groups
ps10 <- as.Date(c("1999-01-01", "2000-05-01"))
pe10 <- as.Date(c("1999-03-01", "2000-05-30"))
pr10 <- data.frame(digest = c('VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'VWW5Z7D6BOLEI7HLJH5BBHYW7QRVEQKK',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                             'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC'),
                  group = c(1, 1, 2, 2))
# and finally test the inverse selection order.
# If n is not set than there should be no difference.
ps11 <- as.Date("2000-01-01")
pe11 <- as.Date("2000-10-01")
pr11 <- data.frame(digest = c('J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE',
                              'J25H2BIPZ32P3423DIPBFD6ZHRJRX5FE',
                              'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                              'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                              'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                              '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q'),
                   group = c(1, 1, 1, 1, 1, 1))
# and the result when I'm using n=3
ps12 <- as.Date("2000-01-01")
pe12 <- as.Date("2000-10-01")
pr12 <- data.frame(digest = c('ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                              'ZTL6F2WUIKFFJSKZOI7XLBUGUNOTEPOC',
                              '5XHLF7WZZ74E6GNQCZRGRFIVPA7MT55Q'),
                   group = c(1, 1, 1))
# and the test code
test_that("Selection based on time period", {
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps1, pe1, 0, F)[, c("digest", "group")], pr1)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps2, pe2, 0, F)[, c("digest", "group")], pr2)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps3, pe3, 0, F)[, c("digest", "group")], pr3)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps4, pe4, 0, F)[, c("digest", "group")], pr4)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps5, pe5, 0, F)[, c("digest", "group")], pr5)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps6, pe6, 0, F)[, c("digest", "group")], pr6)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps7, pe7, 0, F)[, c("digest", "group")], pr7)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps8, pe8, 0, F)[, c("digest", "group")], pr8)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps9, pe9, 2, F)[, c("digest", "group")], pr9)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps10, pe10, 2, F)[, c("digest", "group")], pr10)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps11, pe11, 0, T)[, c("digest", "group")], pr11)
    expect_equal(WaybackFetcheR:::select_snaps_period(cdx, ps12, pe12, 3, T)[, c("digest", "group")], pr12)
})
