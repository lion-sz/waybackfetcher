context("Checking the warning messages")
library(WaybackFetcheR)

d <- as.Date("2000-01-01")
u <- "https://abeking.com"

test_that("Checking of warnings messages for several date options", {
    expect_warning(fetch_wayback(u))
    expect_warning(fetch_wayback(u, date = d, year = d))
    expect_warning(fetch_wayback(u, date = d, start = d, end = d))
    expect_warning(fetch_wayback(u, year = d, start = d, end = d))
})

# and further I want to test that bad parameter in n, path and crawl_delay do not cause crashes.
test_that("Bad parameter warnings", {
    expect_warning(fetch_wayback(u, date = d, n = "a"))
    expect_warning(fetch_wayback(u, date = d, n = -1))
    expect_warning(fetch_wayback(u, date = d, path = 12))
    expect_warning(fetch_wayback(u, date = d, path = "/home/ThisPathSurelyDoesNotExist"))
    expect_warning(fetch_wayback(u, date = d, crawl_delay = "a"))
    expect_warning(fetch_wayback(u, date = d, crawl_delay = -1))
})
