WaybackFetcheR
================

This R package provides an interface to request snapshots from the
[Wayback Machine](https://web.archive.org/). There are three ways to
select the snapshots by the date they were taken:

  - When a date is specified the snapshot closest in time to this date
    is selected. This ensures that there is always exactly one snapshot
    returned, as long as the Wayback Machine indexes this website.
  - When a year is specified the first snapshot in this year will be
    returned. When there is no snapshot for this year the return will ne
    not available, evne if there are snapshots for other years.
  - When a start and end date are specified the function will return all
    snapshots that fall in this time window. If you don’t need all
    snapshots you can specify that only n snapshots should be returned.

All these arguments can be specified as vectors. In this case the
snapshots will be fetched for all arguments and returned in one object.

## Usage

``` r
library(WaybackFetcheR)
target_url <- "http://zew.de"

# fetch the snapshot closest to 1st. January 2020
result <- fetch_wayback(target_url, date = as.Date("2020-01-01"))

# fetch the first snapshot in 1990 and 1991
results <- fetch_wayback(target_url, year = c(as.Date("1990-01-01"),
                                              as.Date("1991-01-01")))

# fetch the first 10 snapshots in 2010
restults <- fetch_wayback(target_url, start = as.Date("2010-01-01"),
                          end = as.Date("2011-01-01"), n = 10)
```

## Local backup

Since this package limits its crawl speed by waiting between two
requests repeaditly crawling the same list of snapshots will waste time.
This might be especially relevant when implementing different versions
of one webscraper. This package has the option to save the result (the
api responses and the snapshots) on the local storage and use this to
speed up repeaded runs.

To enable this behaviour set the variable `path` with the path that in
which the structure should be created:

``` r
result <- fetch_wayback(target_url, date = as.Date("2020-01-01"),
                        path = "/data/wayback_data/")
```

## Output

The output of the function is a dataframe that contains 6 columns:

  - `date`: The date the snapshot was taken. This is just the timestamp,
    but formatted into an R Data object.
  - `html`: The html returned, encoded as a string.
  - `group`: If you provided several dates / timeperiods arguments this
    indicator allows you to identify to which argument the snapshot
    belongs. It is simply the index of the argument it belongs to.
  - `timestamp`: The timestamp of the snapshot as received by the
    Wayback Machine.
  - `digest`: A digest of the website. This is also calculated by the
    Wayback Machine. Identical snapshots will have identical digests and
    therefor this digest can show if a website changed over time.
  - `original`: The URL that the Wayback machine indexed.

If the function encounters an error that makes it impossible to give
meaningful results the function will return `NULL`. If there is an error
specific to a single snapshot the html value will be `NA`.

## Installation

This package is not part of the [cran](https://cran.r-project.org/).
However it can be easily installed directly from
[gitlab](https://about.gitlab.com/), using the package
[devtools](https://www.rdocumentation.org/packages/devtools):

``` r
library(devtools)
install_gitlab("ClF3/waybackfetcher")
```

## How does it work?

To find the appropriate snapshots this package uses the Wayback Machines
[CDX
API](https://github.com/internetarchive/wayback/tree/master/wayback-cdx-server).
The snapshots are then selected and downloaded. Since the Wayback
Machine is smart enough to recognise this package as an bot it omits the
toolbar from the top and all I need to remove are a few javascript
inserts.

## Issues / Comments

For any Issues or Problems use either the packages gitlab page or email
me directly under <lion-sz@gmx.de>.
