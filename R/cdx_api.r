#' CDX API Interface
#'
#' Mangage the CDX API interface and the top level local folder
#'
#' ERROR HANDLING IS NOT IMPLEMENTED
#' This function manages the CDX API call and the initialization of the
#' local folder - if wanted - with the CDX response object.
#' This allows me to run not only the same, but also different queries
#' on the same url without to much extra calls.
#' However local storage is only used if the store_local value is not NULL
#'
#' @param snap_url The url as given by the user.
#'      If it does not start with https or http this is added.
#' @param snap_local The path as given by the user.
#'      It is assumed that this path is viable or NULL.
#' @param latest The latest date of all the given arguments.
#'      If this date is after the last snapshot in a local CDX file,
#'      the file is requested again.
#' @param date_selection This parameter enables date selection.
#'      When this is the case I compare the newest snap locally and
#'      the current date to see if I need a new snap.
#' @param crawl_delay The function waits half this delay before exiting,
#'      if it got information from the wayback machine.
#' @param verbose Should I sent a message when making an API call?
#'
#' @return A list containing two elements:
#'  * cdx: The cdx response table.
#'      NULL if the Website is not availabel.
#'  * local_path: The local path where the html should be saved.
#'      NULL if the html should not be saved
#'
cdx_api <- function(snap_url, snap_local, latest,
                    date_selection, crawl_delay, verbose) {

    # first determine if I should use local storage
    use_local <- !is.null(snap_local)

    # when I want local storage first I need to build the folder structure
    if (use_local) {
        host <- parse_url(snap_url)$hostname
        path <- parse_url(snap_url)$path
        path <- gsub("/", "_", path)
        loc_dir <- paste(snap_local, host, path, sep = "/")
        # I want to flathen the path, so that I don't need recursive listing
        if (!(host %in% list.dirs(snap_local, full.name = F, recursive = F))) {
            dir.create(paste(snap_local, host, sep = "/"), recursive = TRUE)
        }
        # next check if I've parsed this site before.
        if (!(path %in% list.dirs(paste(snap_local, host, sep = "/"), full.name = F))) {
            dir.create(loc_dir, recursive = TRUE)
        }
    }

    # Now I have to check if I need a new copy of the CDX results.
    # This is the case if
    #   a) the local copy is older than the latest date,
    #   b) I work with dates and the newest local snap is closer to latest than
    #       the touched date.
    if (use_local) {
        cdx_cont <- NULL
        loc_files <- list.files(loc_dir)
        # first check fi I've touched this before.
        # Read in the files and check option a.
        if (any(grepl("touched", loc_files))) {
            touched <- as.Date(readLines(paste(loc_dir, "touched", sep = "/")))
            # read in the CDX file. If it doesnt exist there was no snap
            # the last time it was touched.
            if (any(grepl("CDX.txt", loc_files))) {
                # if comment.char is not set, R interpretes '#' as a comment,
                # which breaks the reading ofsome files.
                cdx_cont <- read.table(paste(loc_dir, "CDX.txt", sep = "/"),
                                       header = T, comment.char = "")
                cdx_cont$date <- as.Date(cdx_cont$date)
                req_new_cdx <- latest > touched
            } else {
                req_new_cdx <- FALSE
            }
        } else {
            req_new_cdx <- TRUE
        }
        # now option b. Only if I use dates and dont already need a new cdx
        if (date_selection & !req_new_cdx) {
            # There are two cases when I need a new CDX:
            # 1. When I do not have an old.
            # 2. When the last snap is further away than half the difference
            # between touched and the last snap, there might be a new
            # snap that is better suited for the latest date.
            # Therefor I need a new CDX
            if (is.null(cdx_cont)) {
                req_new_cdx <- TRUE
            } else {
                req_new_cdx <- (latest - max(cdx_cont$date)) >
                                (touched - max(cdx_cont$date)) / 2
            }
        }
    } else {
        req_new_cdx <- TRUE
    }


    # Now I need to query the CDX content if needed.
    if (req_new_cdx) {
        # At this point I can be sure that I'll save the newest cdx response
        # if there is one. Therefor I can write the touched file.
        if (use_local) {
            write(as.character(Sys.time()),
                  paste(loc_dir, "touched", sep = "/"))
        }
        # first check if the URL is inside the wayback machine.
        # This is done using the archive.org/available api
        call_url <- paste0(AVAILABLE_API_BASE, snap_url)

        # if this call fails I simply need to remove the touched file so
        # that the next time the code runs the CDX response is downloaded again
        if (verbose) message(paste("Fetching availability API:", call_url))
        api_res <- tryCatch(GET(call_url),
                 error = function(err){
                    message(paste("check availability failed for", snap_url, err$message))
                    if (use_local) {
                        file.remove(paste(loc_dir, "touched", sep = "/"))
                    }
                    return(NULL)
                 })
        # when the call failed and api_res is null return
        if (is.null(api_res)) {
            return(list(cdx = NULL, local_path = NULL))
        }

        api_cont <- content(api_res)

        # if the contents archived_snapshots length is zero this means there is no
        # snapshot. Therefor return NULL, the main produces nice output.
        if (length(api_cont$archived_snapshots) == 0) {
            return(list(cdx = NULL, local_path = NULL))
        }

        # at this point I can be sure that the dates are possible and that
        # there is at least one snapshot available.
        # Next I need to query the snapshots and select the appropriate ones.
        # To do so I first query the CDX Api to get a list of all snapshots
        # and then pass this on to a function selecting the best ones.
        # These functions are specific for whether I have date, year or period.
        # Since there are sometimes problems with the separator character in
        # the CDX file, I need json output.
        cdx_url <- paste0(CDX_API_BASE, snap_url, "&output=json")

        if (verbose) message(paste("fetching CDX:", cdx_url))
        cdx_res <- tryCatch(GET(cdx_url),
            error = function(err){
                    message(paste("CDX download failed for", snap_url, err$message))
                    if (use_local) {
                        file.remove(paste(loc_dir, "touched", sep = "/"))
                    }
                    return(NULL)
                 })
        # If the get failed return empty.
        if (is.null(cdx_res)) {
            return(list(cdx = NULL, local_path = NULL))
        }

        # Extract the content and cast to dataframe.
        cdx_cont <- content(cdx_res)
        cdx_cont <- do.call(rbind.data.frame, lapply(cdx_cont, unlist))
        names(cdx_cont) <- c("urlkey", "timestamp", "original",
                             "mimetype", "statuscode", "digest", "length")
        # and remove the first line (a header) and problematic liens.
        if (cdx_cont[1, 1] == "urlkey") {
            cdx_cont <- cdx_cont[-1, ]
        }
        # If a line has an empty digest, it can be removed.
        cdx_cont <- cdx_cont[cdx_cont$digest != "-", ]

#        cdx_cont_str <- content(cdx_res)
#        # this returns a space seperated table, cast to data.frame
#        # ignore the '#', which is otherwise interpreted as a comment
#        cdx_cont <- read.table(text = cdx_cont_str, header = F, comment.char = "")

        cdx_cont$timestamp <- as.character(cdx_cont$timestamp)
        # and now cast the timestamps to R date.
        cdx_cont$date <- as.Date(cdx_cont$timestamp, "%Y%m%d")

        # finally - if local used - write to file and write the touched file.
        if (use_local) {
            write.table(cdx_cont,
                        paste(loc_dir, "CDX.txt", sep = "/"),
                        row.names = F)
        }

        # finally I want to sleep a half crawl_delay before continuing.
        Sys.sleep(0.5 * crawl_delay)
    }

    # build and return the list.
    if (use_local) {
        return(list(cdx = cdx_cont,
                    local_path = loc_dir))
    }
    else {
        return(list(cdx = cdx_cont, local_path = NULL))
    }
}
