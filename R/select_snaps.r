#' Select Snaps by Dates
#'
#' This function selects snapshots by closest to the given dates
#'
#' This is an internal function! It is not supposed to be called by user!
#' For each date the snapshot closest to the date is selected.
#' Therefor I can return several identical snapshots, when
#' the website does not change often or the dates are close together.
#' However I ensure that I dont bother the wayback machine for websites
#' that I already have.
select_snaps_date <- function(snaps, snap_dates) {
    # first I need cast the snap date to a vector so that I can loop.
    if (!is.vector(snap_dates)) {
        snap_dates <- c(snap_dates)
    }

    # and this df the snaps I want to return.
    results <- data.frame(timestamp = character(),
                            date = double(), digest = character())
    
    # now loop through the dates and find the closest one
    for (i in 1:length(snap_dates)) {
        # I first calculate the difference to the date and then select
        # all indices where the difference is equal to the minimum
        dat_diff <- abs(snap_dates[i] - snaps$date)
        snap_ind <- which(dat_diff == min(dat_diff))
        # since I dont really know what to do I just throw a warning
        # if there are more than one digests for this date.
        # In this case the first element will be selected.
        if (length(unique(snaps$digest[snap_ind])) > 1) {
            warning(paste("The URL", snaps$original[1], "for", snap_dates[i],
                          "does not have a unique digest! Continuing anyway."))
        }
        # here I need to select the appropriate index to add to the list
        snap_ind <- snap_ind[1]

        # and now add all relevant info to the return df
        results <- rbind(results,
                           snaps[snap_ind, c("timestamp", "date",
                                             "digest", "original")])
    }

    results$group <- 1:length(snap_dates)
    row.names(results) <- NULL

    return(results)
}


#' Select snaps by year
#'
#' Select the first snapshot for every year
#'
#' This function selects the first snapshot for every year and then
#' requests these snaphosts to be fetched.
#' The return is the same as for the function above
select_snaps_year <- function(snaps, snap_years) {
    # first select for every year the first snapshot of that year.
    years <- format(snaps$date, "%Y")
    # since the snapshots are ordered I can simply compare the years
    # and select the first snapshot where the years are equal.
    # Further I need to remember the group of each snapshot.
    snap_sel <- numeric()
    grp <- numeric()
    for (i in 1:length(snap_years)) {
        tmp_year <- format(snap_years[i], "%Y")
        if (any(years == tmp_year)) {
            snap_sel <- append(min(which(years == tmp_year)), snap_sel)
            grp <- append(i, grp)
        }
    }

    # if there is no snap selected return NULL
    if (length(snap_sel) == 0) {
        return(NULL)
    }

    # now that I have the selection I need to build the results data frame.
    results <- snaps[snap_sel, c("timestamp", "date", "digest", "original")]
    results$group <- grp
    row.names(results) <- NULL

    return(results)
}


#' Select snaps by time period
#'
#' This function selects n snaps by time period
#'
#' I calculate the differences for both start and end and then select n snaps.
select_snaps_period <- function(snaps, snaps_start, snaps_end,
                                n, inv_sel_ord) {
    # I want to iterate through the groups and store the selected snaps in an vec
    # and the groups belonging to this in a second vec
    sel <- numeric()
    grp <- numeric()
    for (i in 1:length(snaps_start)) {
        # select all snaps that are in the time window
        diff_start <- snaps_start[i] - snaps$date
        diff_end <- snaps_end[i] - snaps$date
        sel_tmp <- which((0 >= diff_start) & (diff_end >= 0))
        # check if this are no more than n snapshots
        # if n == 0 this behaviour is deactivated
        if ((n > 0) & (length(sel_tmp)) > n) {
            # I need to select the n first (or last) snaps.
            # I can't use an ifelse since it returns only one value.
            if (inv_sel_ord) {
                sel_tmp <- tail(sel_tmp, n)
            }
            else {
                sel_tmp <- head(sel_tmp, n)
            }
        }
        # if there is at least one selected snap add the stuff
        if (length(sel_tmp) > 0) {
            sel <- append(sel, sel_tmp)
            grp <- append(grp, rep(i, length(sel_tmp)))
        }
    }

    # first I need to check that I have any results.
    if (length(sel) == 0) {
        # return NULL since there is no snapshot to be taken
        return(NULL)
    }
    # Now I must build the results data Frame
    results <- snaps[sel, c("date", "timestamp", "original", "digest")]
    results$group <- grp
    row.names(results) <- NULL

    return(results)
}
