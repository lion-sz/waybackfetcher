#' Query Wayback URL
#'
#' This function fetches and prepares the html
#'
#' The function takes a dataframe containing at least timestamps
#' and further the URL.
#' It returns a vector of altered html responses.
#' Fortunately the wayback machine recognizes that I'm a bot and 
#' therefor does not serve me their inserted tool. This means I
#' dont have to clean the response. I can just pass it on.
#'
#' @param snaps The selection data frame. The html for all the entries
#'      will be downloaded or loaded from storage.
#' @param clawl_delay The delay between two calls to the wayback machine.
#' @param path The file path returned by the cdx_api function.
#'      This path will be checked for the local file.
#' @param verbose Should I sent a message when making an api call?
#'
#' @return the snaps data frame with the html col added.
#'
get_sites_wayback <- function(snaps, crawl_delay, path, verbose) {
    # for every snap I want to build the url, then get a request
    # and check the response code. If there is a Error add NULL ot output
    # If the status is not Success I add NULL and write a warning
    results <- character()
    # first I want to get all files
    if (!is.null(path)) {
        files <- list.files(path)
    }

    for (i in 1:dim(snaps)[1]) {
        # first I want to try the local sorage if it's enabled
        fetch_url <- TRUE
        if (!is.null(path)) {
            if (any(grepl(paste0(snaps$digest[i], ".htm"), files, fixed = T))) {
                # read in the file and append to the results
                res <- readLines(paste0(path, "/",
                                        grep(snaps$digest[i], files,
                                              value = T, fixed = T)))
                res <- paste(res, collapse = "\n")
                results <- append(results, res)
                fetch_url <- FALSE
            } else if (any(grepl(paste0(snaps$digest[i], "_err.htm"), files, fixed = T))) {
                # is there an error snapshot.
                if (verbose) message("Using cached http error status!")
                results <- append(results, NA)
                fetch_url <- FALSE
            }
        }
        
        # if I still need the URL fetch it.
        if (fetch_url) {
            res_url <- paste0(WAYBACK_URL, snaps$timestamp[i],
                              "/", snaps$original[i])

            # If I get an error message I append NA and continue.
            if (verbose) message(paste("fetching snap:", res_url))
            res <- tryCatch(GET(res_url),
                error = function(err) {
                    message(paste(err$message, "in", fetch_url))
                    return(NA)
                })
            # I need to check if I encountered an error.
            # If so I append NA and continue.
            # This clause means that if I've encountered an error e.g. in communication with the wayback
            # machine or a general networking error, there is no error file written.
            if (length(res) == 1 && is.na(res)) {
                results <- append(results, NA)
                Sys.sleep(crawl_delay)
                next
            }
            if (http_status(res)$reason != "OK") {
                warning(http_status(res)$message)
                results <- append(results, NA)
                # check if there is a Wayback JS include. If this is the case I've found a website that returned
                # an error to the wayback machine. Therefore I write this into a separate file.
                cont <- content(res, as = "text")
                if (grepl("<head><script(.|\n)*<!-- End Wayback Rewrite JS Include -->", cont)) {
                    if (!is.null(path)) {
                        sub("<head><script(.|\n)*<!-- End Wayback Rewrite JS Include -->", "<head>", cont)
                        write(cont, paste0(path, "/", snaps$digest[i], "_err.htm"))
                    }
                }
            }
            else {
                cont <- content(res, as = "text")
                # remove the unneeded js inputted by the wayback machine
                sub("<head><script(.|\n)*<!-- End Wayback Rewrite JS Include -->", "<head>", cont)
                results <- append(results, cont)
                # and if local is enabled write to local storage
                if (!is.null(path)) {
                    write(cont, paste0(path, "/", snaps$digest[i], ".htm"))
                }
            }

            Sys.sleep(crawl_delay)
        }
    }
    
    return(results)
}
