#' Is this a Date?
#'
#' This function returns whether x is a date
#'
#' Since R has no function to do so I have to copy this from Stackoverflow
#' myself.
#' I test if the object given as arguments inherits from date.
#' If the object is a vector the function returns true if all
#' elements are Dates.
#' To be honest this doesnt really need a new file, but a one-file-package
#' looks rather lame.
#'
#'
#' @param x The object that should be tested for Date
#' @return bool Whether the object x is date.
#'
is.date <- function(x) {
    if (is.vector(x)){
        return(all(sapply(x, inherits, "Date")))
    }
    else {
        return(inherits(x, "Date"))
    }
}
