#' Wayback Machine Fetcher
#'
#' This function fetches an URL at a specified timepoints
#' (or the closests available) from the Wayback Machine.
#'
#' This library uses the Wayback machines API to query information
#' on the available snapshots, which are then selected based on the
#' parameter specified.
#'
#' It supports three ways of selecting the snapshots. Please note that
#' you must specify exactly one of these three options, otherwise the
#' program will stop!
#'
#' The time arguments can be either a date or a vector of Dates.
#' If vectors were provided the function will return several snapshots.
#' The return value indicates a group for each snapshot. This group is
#' the index of the time argument it belongs to.
#'
#' When called with a date, the snapshot that is closest in time will be
#' selected. Therefor it is guaranteed that every date will return exactly
#' one snapshot, if the side is indexed at all. However this might return
#' snapshots that were created years after the specified date.
#'
#' Calling the function with a year will select the first snapshot from this
#' year. If there is no snapshot for this year, there will be no return
#' (and the return object might be NULL).
#'
#' Finally one can select a start and end date. When done the function
#' will pick the first n snapshots between these two dates.
#' By default the function will return all snapshots between these dates.
#'
#' To prevent unneeded load to the archive.org server this function ensures
#' that it does not download an identical document several times.
#' This is done using the digest provided by the wayback machine cdx API. 
#'
#' Further this library can store a local copy of the donwloaded data.
#' This is useful since it enables the user to repeatedly parse the same
#' sites without having to donload all sites again or manually manage local
#' copies. This is enabled by providing a path.
#'
#'
#' @param snap_url The URL that should be downloaded
#'      If this is not a full url it will be prefixed with 'http://'
#' @param date When provided the snapshot closest to this date will be
#'  returned.
#' @param year When provided the first snapshot from this year will
#'  be returned.
#' @param start Must be specified toghether with the end date.
#'  If these dates are provided the snapshot returned will lie between these.
#' @param end The end date
#' @param n When start and end date are specified this parameter allows the
#'  user to select the number of snapshots that should be returned.
#'  By default this is zero in which case all snapshots are returned.
#' @param inv_sel_ord This option inverts the selection order (last snap first)
#'      when there are more than n snapshots in one time period.
#' @param crawl_delay The delay between two calls to the side. This is set
#'      to one second by default, which should be a sane default.
#' @param path If this variable is set the function will look for and store a
#'      local copy of the both the metadata and the actual html site fetched
#'      in the specified path.
#' @param verbose Should I sent a message when a website has no snapshots
#'      available and whenever making an api call?
#'      Usefull for debuggin.
#'
#' @return
#' Data Frame containing
#'  1. group: The index of the time argument this snap belongs to.
#'      This is only relevant for when snaps are selected by years.
#'      For all other modes this is meerely the an increasing counter.
#'  2. date: The date of the response as an R object
#'  3. timestamp: The timestamp in the format used by the wayback machine
#'  4. original: The original url that is fetched.
#'  5. digest: The digest of the website as provided
#'  6. html: The HTML encoded as string
#' The return might be NULL if there was an erroro.
#'  the html might be NA if there was a problem specific to this page.
#' @export
fetch_wayback <- function(snap_url, date = NULL, year = NULL,
                          start = NULL, end = NULL, n = 0,
                          crawl_delay = 1, inv_sel_ord = FALSE,
                          path = NULL, verbose = FALSE) {

    # Since I want to run the httr::parse_url function I need to ensure,
    # that the URL has a scheme. If this is not the case append a http.
    if (! grepl("*?://", snap_url)) {
        snap_url <- paste0("http://", snap_url)
    }
    # next I want to check that I support the protocol. This is the case for http(s).
    if (! grepl("https?://", snap_url)) {
        warning("The scheme is not supported (only http and https are)!")
        return(NULL)
    }

    # check if the path exists
    if (!is.null(path)) {
        if (!is.character(path)) {
            warning("path is not a string!")
            return(NULL)
        }
        if (!file.exists(path)) {
            warning("path does not exist!")
            return(NULL)
        }
    }

    # and check if the n argument is greater or equal to zero
    if (!is.numeric(n) | n < 0) {
        warning("n must be a non-negative number!")
        return(NULL)
    }

    # and that the crawl_delay is positive
    if (!is.numeric(crawl_delay) | crawl_delay < 0) {
        warning("crawl_delay must be a non-negative number!")
        return(NULL)
    }

    # next I have to check that the arguments are compatible.
    # This is only the case if exactly one of the date,year,(start,end) is set.
    correct_args <- FALSE
    if (!is.null(date)) {
        if (!is.date(date)) {
            # when the argument is not a date just exit
            warning("date is not Date format!")
            return(NULL)
        }
        correct_args <- TRUE
        # Here I take the last date, even though I might need snapshots newer
        # than this date. This is done by enbaling some special treatment for
        # date in the cdx_api function.
        latest_date <- max(date)
    }
    if (!is.null(year)) {
        # I need to check that the argument is date and that I  correct_args isnt true.
        if (correct_args) {
            # stop execution because two arguments were given.
            warning("Both date and year were specified!")
            return(NULL)
        }
        if (!is.date(year)) {
            # stop execution
            warning("year is not exclusively Date!")
            return(NULL)
        }
        correct_args <- TRUE
        latest_date <- ceiling_date(max(year), unit = "year")
    }
    if (!is.null(start) & !is.null(end)) {
        if (!(is.date(start) & is.date(end))) {
            # when not both are just dates stop execution
            warning("Either start or end was not exclusively dates")
            return(NULL)
        }
        if (correct_args) {
            warning("multiple conflicting arguments set!")
            return(NULL)
        }
        correct_args <- TRUE
        latest_date <- max(end)
    }
    if (!correct_args) {
        # there was no correct argument given
        warning("There was no correct argument given.")
        return(NULL)
    }
    
    cdx_res <- cdx_api(snap_url, path, latest_date,
                       !is.null(date), crawl_delay, verbose)

    # check that there are any snapshots. If not return NULL
    if (is.null(cdx_res$cdx)) {
        if (verbose) message("There are no snapshots")
        return(NULL)
    }

    # and next I need to call the function for selecting the best snaps
    if (!is.null(date)) {
        snaps <- select_snaps_date(cdx_res$cdx, date)
    }
    else if (!is.null(year)) {
        snaps <- select_snaps_year(cdx_res$cdx, year)
    }
    else {
        snaps <- select_snaps_period(cdx_res$cdx, start, end,
                                     n, inv_sel_ord)
    }

    # check if there are any snaps selected, if not return NULL
    if (is.null(snaps)) {
        if (verbose) message("No appropriate snapshots found!")
        return(NULL)
    }

    # finally get the actual snapshots and merge them to the results
    to_fetch <- snaps[!duplicated(snaps$digest), ]
    # fetch these sites
    fetched <- get_sites_wayback(to_fetch, crawl_delay, cdx_res$local_path, verbose)
    # and build the resutls html array from it
    snaps$html <- unlist(sapply(snaps$digest,
                           function(x) fetched[to_fetch$digest == x]))

# finally I need to build the return list from the snaps and go my merry way.
    return(snaps)
}
