#' Availability API
#'
#' This API is used to determine whether a URL is being snapshotted by
#' the wayback machine.
#' It is always called first.
AVAILABLE_API_BASE <- "https://archive.org/wayback/available?url="

#' CDX API
#'
#' An API to request metadata of all snapshots
#'
#' This API allows me to get a list of all snapshots of the url.
#' Therefor I call this once, request a json object and then
#' select the best Snapshots.
CDX_API_BASE <- "https://web.archive.org/cdx/search/cdx?&url="

#' Wayback URL
#'
#' This URL is used to actually request a wayback site.
WAYBACK_URL <- "https://web.archive.org/web/"
